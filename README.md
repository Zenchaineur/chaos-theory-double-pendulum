# Chaos Theory Double Pendulum

This project aims to explain the Chaos Theorem through the example of the double pendulum example. This project is part of an exhibition for the seminar of the math-museum of the University of Passau. 

## Getting started

Go to : https://zenchaineur.gitlab.io/chaos-theory-double-pendulum/

Explaining :

Click on one of the buttons to display the corresponding pendulum screen.
Each of them contains two pendulum that move themselves at the same time ;
below them is displayed their distance.
The goal here is to understand the chaotic behaviour of the double pendulum :
You can reduce the ratio as much as you like, there will always be a point where the distance starts to diverge,
whereas it will only be sinusoidal for the simple pendulum.
Double pendulum :
Prompt the values for the mass, length and angle of each pendulum.
The ratio field corresponds to the rates by which the angles are multiplied for the second double pendulum.

Simple pendulum : Prompt the values for the mass, length and angle of the pendulum.
The ratio field corresponds to the rates by which the angle is multiplied for the second simple pendulum.

## Authors and acknowledgment

Project led and directed by Mario Gancarski, and under the guidance of Elena Mille, Hella Epperlein and Brigitte Forster.

## Quote

"_Relativity eliminated the Newtonian illusion of absolute space and time; quantum theory eliminated the dream of a controllable measurement process. Chaos eliminates the Laplacian utopia of deterministic predictability._"

James Gleick.

