var dist_abs;
var new_dist_abs;
var init_double = false;
var init_simple = false;

var g;
var h;
var t;

var r1;
var r2;
var m_1;
var m_2;
var x1;
var y1;
var x2;
var y2;
var th1;
var th2;
var om1;
var om2;
var dom_1;
var dom_2;

var k1;
var j1;
var k2;
var j2;
var k3;
var j3;
var k4;
var j4;



var bisx1;
var bisy1;
var bisx2;
var bisy2;
var bisth1;
var bisth2;
var bisom1;
var bisom2;
var bisdom1;
var bisdom2;

var bisk1;
var bisj1;
var bisk2;
var bisj2;
var bisk3;
var bisj3;
var bisk4;
var bisj4;

var myp5_1;
var myp5_2;
var myp5_3;

var l;
var m;
var th;
var om;
var thbis;
var ombis;
var x;
var y;
var xbis;
var ybis;

var dom;

var dom1_compute;

var dom2_compute;
  
var token = 0;


function resetDouble() {
  var val_r1 = float(inp_r1.value());
  var val_r2 = float(inp_r2.value());
  var val_m1 = float(inp_m1.value());
  var val_m2 = float(inp_m2.value());
  var val_th1 = float(inp_th1.value());
  var val_th2 = float(inp_th2.value());
  var val_bisth1 = float(float(inp_th1.value())*float(inp_ratiod.value()));
  var val_bisth2 = float(float(inp_th2.value())*float(inp_ratiod.value()));
  r1 = val_r1;
  r2 = val_r2;
  m_1 = val_m1;
  m_2 = val_m2;
  th1 = val_th1;
  th2 = val_th2;
  bisth1 = val_bisth1;
  bisth2 = val_bisth2;
  h = 0.3
  t = 0
  om1 = 0
  om2 = 0
  dom_1 = 0
  dom_2 = 0
  bisom1 = 0
  bisom2 = 0
  bisdom1 = 0
  bisdom2 = 0

  token += 1
  
  myp5_1 = new p5(s1double, "p5sketch1double");
  myp5_2 = new p5(s2double, "p5sketch2double");


  dom1_compute = function (t,om1,om2,th1,th2) {
    return (-m_2*r1*Math.sin(th1-th2)*Math.cos(th1-th2)*om1*om1-m_2*r2*Math.sin(th1-th2)*om2*om2-m_1*g*Math.sin(th1-th2)*Math.cos(th2))/(r1*(m_1+m_2*(Math.sin(th1-th2))*(Math.sin(th1-th2))))
  }

  dom2_compute = function (t,om1,om2,th1,th2) {
    return ((m_1+m_2)*r1*Math.sin(th1-th2)*om1*om1+m_2*r2*Math.sin(th1-th2)*Math.cos(th1-th2)*om2*om2+(m_1+m_2)*g*Math.sin(th1-th2)*Math.cos(th1))/(r2*(m_1+m_2*(Math.sin(th1-th2))*(Math.sin(th1-th2))))
  }

}

function resetSimple() {
  var val_l = float(inp_l.value());
  var val_m = float(inp_m.value());
  var val_th = float(inp_th.value());
  var val_thbis = float(float(inp_th.value())*float(inp_ratio.value()));
  l = val_l;
  m = val_m;
  th = val_th;
  thbis = val_thbis;
  t = 0
  om = 0
  ombis = 0

  
  token += 1
  
  myp5_1 = new p5(s1simple, "p5sketch1simple");
  myp5_2 = new p5(s2simple, "p5sketch2simple");

  dom = function (theta) {
    return (-g/l*Math.sin(theta))
  }
}


function setup() {

}
  
function draw() {
}


var s1simple = function (sketch) {
  var self_token;
  var cleared;
  sketch.setup = function () {
    cx = 500;
    cy = 150;
    self_token = token;
    var splpdl_cnv = sketch.createCanvas(1200, 400);
    splpdl_cnv.position(100, 50);
    sketch.createP("");
    cleared = false;
  };

  sketch.draw = function () {
    sketch.background(300);
    sketch.translate(cx, cy);
    
    if(self_token == token){
      om = om + dom(th)
      th = th + (h*om)
      x = Math.sin(th) * l
      y = Math.cos(th) * l

      if (om > 1){
        om = 1
      }

      sketch.line(0, 0, x, y);
      sketch.fill(0);
      sketch.ellipse(x, y, m, m);
      
      
      ombis = ombis + dom(thbis)
      thbis = thbis + (h*ombis)
      xbis = Math.sin(thbis) * l
      ybis = Math.cos(thbis) * l

      if (ombis > 1){
        ombis = 1
      }
      sketch.line(0, 0, xbis, ybis);
      sketch.fill(0);
      sketch.ellipse(xbis, ybis, m, m);
      
      t = t + h

    }
    else if (!cleared){
      sketch.remove()
      cleared = true;
    }
  };
};

var s2simple = function (sketch) {
  var self_token;
  var cleared;
  var dist_abs = 0;
  var new_dist_abs = 0;
  sketch.setup = function () {
    self_token = token;
    var dist_cnv = sketch.createCanvas(4000, 500);
    dist_cnv.position(0, 500);
    sketch.createP("");
    cleared = false;
  };

  sketch.draw = function () {
    if (self_token == token){
      sketch.translate(0, 150);
      dist_abs = new_dist_abs;
      new_dist_abs = Math.sqrt(
        (om - ombis) ** 2 +  (th - thbis) ** 2
      );
      sketch.line(
        sketch.frameCount,
        -dist_abs,
        sketch.frameCount + 1,
        -new_dist_abs
      );
    }
    else if (!cleared){
      sketch.remove()
      cleared = true;
    }
  };
};

var s3simple = function (sketch) {

  buttons = createButton('change values')
  buttons.position(40, 260);
  buttons.mousePressed(resetSimple);

  inp_l = createInput('120')
  inp_l.position(40, 100);
  inp_l.size(100);

  inp_m = createInput('25')
  inp_m.position(40, 140);
  inp_m.size(100);
  
  inp_th = createInput('2.5')
  inp_th.position(40, 180);
  inp_th.size(100);
  
  inp_ratio = createInput('1.1')
  inp_ratio.position(40, 220);
  inp_ratio.size(100);

  sketch.setup = function () {
    var button_cnv = sketch.createCanvas(40, 300);
    button_cnv.position(0, 105);
    sketch.createP("");

    sketch.text("l :", 5, 10)
    sketch.text("m :", 5, 50)
    sketch.text("th :", 5, 90)
    sketch.text("ratio :", 5, 130)
      
    buttons.show();
    inp_l.show();
    inp_m.show();
    inp_th.show();
    inp_ratio.show();

  };

  sketch.draw = function () {
    if(!init_simple){
      sketch.remove()
    }
  };
};


var s1double = function (sketch) {

  var self_token;
  var cleared;
  sketch.setup = function () {
    cx = 500;
    cy = 150;
    self_token = token;
    cleared = false
    var dblpdl_cnv = sketch.createCanvas(1200, 400);
    dblpdl_cnv.position(100, 50);
    sketch.createP("");
  };

  sketch.draw = function () {
    sketch.background(300);
    sketch.translate(cx, cy);
    if (self_token == token){

      k1 = h * dom1_compute (t, om1, om2, th1, th2)
      j1 = h * dom2_compute (t, om1, om2, th1, th2)
      k2 = h * dom1_compute (t+h/2, om1+h*k1/2, om2+h*j1/2, th1, th2)
      j2 = h * dom2_compute (t+h/2, om1+h*k1/2, om2+h*j1/2, th1, th2)
      k3 = h * dom1_compute (t+h/2, om1+h*k2/2, om2+h*j2/2, th1, th2)
      j3 = h * dom2_compute (t+h/2, om1+h*k2/2, om2+h*j2/2, th1, th2)
      k4 = h * dom1_compute (t+h, om1+h*k3, om2+h*j2, th1, th2)
      j4 = h * dom2_compute (t+h, om1+h*k3, om2+h*j2, th1, th2)

      dom_1 = (k1/6) + (k2/3) + (k3/3) + (k4/6)
      dom_2 = (j1/6) + (j2/3) + (j3/3) + (j4/6)
      
      om1 = om1 + dom_1
      om2 = om2 + dom_2
      
      if (om1 > 0.4){
        om1 = 0.4
      }
      if (om1 < -0.4){
        om1 = -0.4
      }

      th1 = th1 + (h*om1)
      th2 = th2 + (h*om2)


      x1 = Math.sin(th1) * r1
      y1 = Math.cos(th1) * r1
      x2 = x1 + Math.sin(th2) * r2
      y2 = y1 + Math.cos(th2) * r2

      sketch.line(0, 0, x1, y1);
      sketch.fill(0);
      sketch.ellipse(x1, y1, m_1, m_1);

      sketch.line(x1, y1, x2, y2);
      sketch.ellipse(x2, y2, m_2, m_2);

      
      bisk1 = h * dom1_compute (t, bisom1, bisom2, bisth1, bisth2)
      bisj1 = h * dom2_compute (t, bisom1, bisom2, bisth1, bisth2)
      bisk2 = h * dom1_compute (t+h/2, bisom1+h*bisk1/2, bisom2+h*bisj1/2, bisth1, bisth2)
      bisj2 = h * dom2_compute (t+h/2, bisom1+h*bisk1/2, bisom2+h*bisj1/2, bisth1, bisth2)
      bisk3 = h * dom1_compute (t+h/2, bisom1+h*bisk2/2, bisom2+h*bisj2/2, bisth1, bisth2)
      bisj3 = h * dom2_compute (t+h/2, bisom1+h*bisk2/2, bisom2+h*bisj2/2, bisth1, bisth2)
      bisk4 = h * dom1_compute (t+h, bisom1+h*bisk3, bisom2+h*bisj2, bisth1, bisth2)
      bisj4 = h * dom2_compute (t+h, bisom1+h*bisk3, bisom2+h*bisj2, bisth1, bisth2)

      bisdom1 = (bisk1/6) + (bisk2/3) + (bisk3/3) + (bisk4/6)
      bisdom2 = (bisj1/6) + (bisj2/3) + (bisj3/3) + (bisj4/6)
      
      bisom1 = bisom1 + bisdom1
      bisom2 = bisom2 + bisdom2
      
      if (bisom1 > 0.4){
        bisom1 = 0.4
      }
      if (bisom1 < -0.4){
        bisom1 = -0.4
      }

      bisth1 = bisth1 + (h*bisom1)
      bisth2 = bisth2 + (h*bisom2)


      bisx1 = Math.sin(bisth1) * r1
      bisy1 = Math.cos(bisth1) * r1
      bisx2 = bisx1 + Math.sin(bisth2) * r2
      bisy2 = bisy1 + Math.cos(bisth2) * r2



      sketch.line(0, 0, bisx1, bisy1);
      sketch.fill(0);
      sketch.ellipse(bisx1, bisy1, m_1, m_1);

      sketch.line(bisx1, bisy1, bisx2, bisy2);
      sketch.ellipse(bisx2, bisy2, m_2, m_2);
      
      t = t + h

      dist_abs = new_dist_abs;
      new_dist_abs = Math.sqrt(
        (om1 - bisom1) ** 2 + (th1 - bisth1) ** 2 + (om2 - bisom2) ** 2 + (th2 - bisth2) ** 2
      );
    }
    else if (!cleared){
      sketch.remove()
      cleared = true;
    }
  };
};


var s2double = function (sketch) {
  var self_token;
  var cleared;
  sketch.setup = function () {
    self_token = token;
    cleared = false;
    var dist_cnv = sketch.createCanvas(4000, 500);
    dist_cnv.position(0, 500);
    sketch.createP("");
  };

  sketch.draw = function () {
    if (self_token == token){
      sketch.translate(0, 150);
      sketch.line(
        sketch.frameCount,
        -2*dist_abs,
        sketch.frameCount + 1,
        -2*new_dist_abs
      );
    }
    else if (!cleared){
      sketch.remove()
      cleared = true;
    }
  };
};

var s3double = function (sketch) {


  button1 = createButton('change values')
  button1.position(40, 380);
  button1.mousePressed(resetDouble);

  inp_r1 = createInput('120')
  inp_r1.position(40, 100);
  inp_r1.size(100);

  inp_r2 = createInput('50')
  inp_r2.position(40, 140);
  inp_r2.size(100);

  inp_m1 = createInput('25')
  inp_m1.position(40, 180);
  inp_m1.size(100);

  inp_m2 = createInput('10')
  inp_m2.position(40, 220);
  inp_m2.size(100);

  inp_th1 = createInput('2.5')
  inp_th1.position(40, 260);
  inp_th1.size(100);

  inp_th2 = createInput('1.5')
  inp_th2.position(40, 300);
  inp_th2.size(100);

  inp_ratiod = createInput('1.1')
  inp_ratiod.position(40, 340);
  inp_ratiod.size(100);

  sketch.setup = function () {
    var button_cnv = sketch.createCanvas(40, 300);
    button_cnv.position(0, 105);

    sketch.createP("");
    sketch.text("r1 :", 5, 10)
    sketch.text("r2 :", 5, 50)
    sketch.text("m1 :", 5, 90)
    sketch.text("m2 :", 5, 130)
    sketch.text("th1 :", 5, 170)
    sketch.text("th2 :", 5, 210)
    sketch.text("ratio :", 5, 250)

    button1.show();
    inp_r1.show();
    inp_r2.show();
    inp_m1.show();
    inp_m2.show();
    inp_th1.show();
    inp_th2.show();
    inp_ratiod.show();
  };


  sketch.draw = function () {
    if(!init_double){
      sketch.remove()
    }
  };
};

function initDoublePendulum(){
  userStartAudio();

  if (init_simple){
    buttons.hide();
    inp_l.hide();
    inp_m.hide();
    inp_th.hide();
    inp_ratio.hide();
  }

  if (!init_double){
    dist_abs = 0;
    new_dist_abs = 0;
    g = 9.81;
    h = 0.3
    t = 0
    r1 = 120;
    r2 = 50;
    m_1 = 25;
    m_2 = 10;
    th1 = 1.5
    th2 = 2.5
    om1 = 0
    om2 = 0
    dom_1 = 0
    dom_2 = 0
    bisth1 = 1.51
    bisth2 = 2.51
    bisom1 = 0
    bisom2 = 0
    bisdom1 = 0
    bisdom2 = 0

    token += 1;

    myp5_1 = new p5(s1double, "p5sketch1double");
    myp5_2 = new p5(s2double, "p5sketch2double");
    myp5_3 = new p5(s3double, "p5sketch3double");

    dom1_compute = function (t,om1,om2,th1,th2) {
      return (-m_2*r1*Math.sin(th1-th2)*Math.cos(th1-th2)*om1*om1-m_2*r2*Math.sin(th1-th2)*om2*om2-m_1*g*Math.sin(th1-th2)*Math.cos(th2))/(r1*(m_1+m_2*(Math.sin(th1-th2))*(Math.sin(th1-th2))))
    }

    dom2_compute = function (t,om1,om2,th1,th2) {
      return ((m_1+m_2)*r1*Math.sin(th1-th2)*om1*om1+m_2*r2*Math.sin(th1-th2)*Math.cos(th1-th2)*om2*om2+(m_1+m_2)*g*Math.sin(th1-th2)*Math.cos(th1))/(r2*(m_1+m_2*(Math.sin(th1-th2))*(Math.sin(th1-th2))))
    }

  }
  init_simple = false;
  init_double = true;
    
}


function initSimplePendulum(){
  userStartAudio();

  if (init_double){
    button1.hide();
    inp_r1.hide();
    inp_r2.hide();
    inp_m1.hide();
    inp_m2.hide();
    inp_th1.hide();
    inp_th2.hide();
    inp_ratiod.hide();
  }
  if (!init_simple){

    g = 9.81;
    h = 0.2
    t = 0;
    l=120;
    m=15;
    th=1.5;
    om = 0;
    thbis=1.6;
    ombis = 0;

    token += 1;

    myp5_1 = new p5(s1simple, "p5sketch1simple");
    myp5_2 = new p5(s2simple, "p5sketch2simple");
    myp5_3 = new p5(s3simple, "p5sketch3simple");

    dom = function (theta) {
      return (-g/l*Math.sin(theta))
    }
  }
  init_simple = true;
  init_double = false;

}


function stop(){

  if (init_double){
    button1.hide();
    inp_r1.hide();
    inp_r2.hide();
    inp_m1.hide();
    inp_m2.hide();
    inp_th1.hide();
    inp_th2.hide();
    inp_ratiod.hide();
  }
  if (init_simple){
    buttons.hide();
    inp_l.hide();
    inp_m.hide();
    inp_th.hide();
    inp_ratio.hide();
  }

  token +=1;
  init_simple = false;
  init_double = false;
}